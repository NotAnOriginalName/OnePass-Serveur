'use strict';

module.exports = function(app) {

  let controller = require('./controller.js');

  app.route('/register/:username/:hashedpassword')
  	.get(controller.check_user)
    .post(controller.create_user);

  app.route('/password/:username/:hashedpassword/:website')
    .get(controller.get_password)
    .put(controller.update_password)
    .delete(controller.delete_password);

  app.route('/password/:username/:hashedpassword/:website/:password')
  	.post(controller.save_password)

};