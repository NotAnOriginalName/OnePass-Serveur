'use strict';


let mongoose = require("mongoose"),
  User = mongoose.model("Users"),
  Password = mongoose.model("Passwords");

exports.create_user = (req, res) => {
  let new_user = new User({username: req.params.username, hashedpassword: req.params.hashedpassword});
  new_user.save( (err, user) => {
    console.log("err : "+err+" user : "+user);
    if (err)
      res.send(err);
    res.json(user);
  });
};

exports.check_user = (req, res) => {

};

exports.get_password = (req, res) => {

};

exports.update_password = (req, res) => {

};

exports.delete_password = (req, res) => {

};

exports.save_password = (req, res) => {

};