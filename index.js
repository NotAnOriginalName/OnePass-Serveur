let express = require("express"),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require("mongoose"),
  tables = require("./api/model"),
  bodyParser = require("body-parser");

let mongoUri = "mongodb://Xavier:test@cluster0-shard-00-00-2doth.mongodb.net:27017,cluster0-shard-00-01-2doth.mongodb.net:27017,cluster0-shard-00-02-2doth.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";

mongoose.connect(mongoUri, {useMongoClient: true})
.then((db) => {
	console.log("started");
	//db.close();
})
.catch((e) => {
	console.log("error : "+e);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use( (req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*"); 
	next();
});

var routes = require("./api/routes");
routes(app);

app.listen(port);

console.log("RESTful API server started on: " + port);