'use strict';

let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let UserSchema = new Schema({
  username: {
    type: String,
    unique : true,
    required: true,
    dropDups: true
  },
  hashedpassword: {
    type: String,
    required: true
  }
});

let PasswordSchema = new Schema({
  username: {
    type: String
  },
  website: {
    type: String
  },
  password: {
    type: String
  }
});

module.exports.Users = mongoose.model('Users', UserSchema);
module.exports.Passwords = mongoose.model('Passwords', PasswordSchema);